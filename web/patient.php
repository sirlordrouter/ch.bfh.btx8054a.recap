<?php
 session_start();

// Test if the user is logged in.
// If no : back to the login page!
if(!isset($_SESSION['staffID'])){
  header('location: index.php');
  exit;
 }
$temp = array();
$pulse = array();
$act = array();
$blood = array ();


include('pdo.inc.php');

try {
    $dbh = new PDO("mysql:host=$hostname;dbname=$dbname", $username, $password);

    $patientID = (int)($_GET['id']);
    if($patientID >0){

	      $sql0 = "SELECT name, first_name
	  FROM patient
	  WHERE patient.patientID = :patientID";
	
	    $statement0 = $dbh->prepare($sql0);
	    $statement0->bindParam(':patientID', $patientID, PDO::PARAM_INT);
	    $result0 = $statement0->execute();

	    while($line = $statement0->fetch()){
	      
	      echo "<h2> Patient : ".$line['first_name']."  ".$line['name']."</h2>";
	
	      echo "<br>\n";
	    }
	
	      /*** echo a message saying we have connected ***/
	      $sql = "SELECT name, first_name, value, time, sign_name, note
	  FROM patient, vital_sign, sign
	  WHERE patient.patientID = vital_sign.patientID
	    AND vital_sign.signID = sign.signID 
	    AND patient.patientID = :patientID
	    ORDER BY sign_name, time";
	
	    $statement = $dbh->prepare($sql);
	    $statement->bindParam(':patientID', $patientID, PDO::PARAM_INT);
	    $result = $statement->execute();
	
	echo "<h3> Signs List </h3>";
	echo "<table class='hor-zebra'>

	<!-- Table header -->
	
		<thead>
			<tr>
				<th scope='col'>Date</th>
				<th scope='col'>Sign</th>
				<th scope='col'>Value</th>
			</tr>
		</thead><tbody>";
			
			echo "<!-- Table body -->";
	
			$last = "Temperature";
			$class = "";
			$odd = "class='odd'";
	    while($line = $statement->fetch()) {
	    	
	    	if($line['sign_name'] <> $last) {
		    	if($class == "") {
			    	$class = $odd;
		    	} else {
			    	$class = "";
		    	}
		    	$last = $line['sign_name'];
	    	} 	      
	      
	      echo "<tr " . $class . " >";
				echo "<td>" . $line['time'] . "</td>";
				echo "<td>" . $line['sign_name'] . "</td>";
				echo "<td>" . $line['value'] . "</td>";
	      echo "</tr>";
			}
			
			echo "</tbody>	<!-- Table footer -->
						<tfoot>
							<tr>
	              <td></td>
	              <td></td>
	              <td></td>
							</tr>
						</tfoot>
						</table>";		
			echo "<br /><h3> Medicine :</h3>";
    echo "<br>\n";
       	
    /*** echo a message saying we have connected ***/
		$sql = "SELECT medicament_name, quantity, time, note, 
		concat(s.name, \", \", s.first_name) as nurse,
		concat(p.name, \", \", p.first_name) as physician
		FROM medicine m
		left join medicament d on m.medicamentID = d.medicamentID
		left join staff s on s.staffID = m.staffID_nurse
		left join staff p on p.staffID = m.staffID_physician
		WHERE patientID = :patientID
		order by time";
    $statement = $dbh->prepare($sql);
    $statement->bindParam(':patientID', $patientID, PDO::PARAM_INT);
    $result = $statement->execute();
    
    echo "<table class='hor-zebra'>

	<!-- Table header -->
	
		<thead>
			<tr>
				<th scope='col'>Name</th>
				<th scope='col'>Quantity</th>
				<th scope='col'>Note</th>
				<th scope='col'>Nurse</th>
				<th scope='col'>Physician</th>
			</tr>
		</thead><tbody>";
			
			echo "<!-- Table body -->";
       
    while($line = $statement->fetch()){
    	  echo "<tr>";
    	  echo "<td>" . $line['medicament_name'] . "</td>";
				echo "<td>" . $line['quantity'] . "</td>";				
				echo "<td>" . $line['note'] . "</td>";
				echo "<td>" . $line['nurse'] . "</td>";
					echo "<td>" . $line['physician'] . "</td>";
	      echo "</tr>";
    }


		echo "</tbody>	<!-- Table footer -->
						<tfoot>
							<tr>
	              <td></td>
	              <td></td>
	              <td></td>
	              <td></td>
	              <td></td>
							</tr>
						</tfoot>
						</table>";

    }
    else{
      echo "<h1>The patient does not exist</h1>";
    }
           
    //echo "</div>";

    $dbh = null;
}
catch(PDOException $e)
{

    /*** echo the sql statement and error message ***/
    echo $e->getMessage();
}


?> 