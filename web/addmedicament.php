<?php
    
session_start();
$medname = $_GET['medname'];
$unit= $_GET['unit'];
include('pdo.inc.php');

try {
$dbh = new PDO("mysql:host=$hostname;dbname=$dbname", $username, $password);
/*** echo a message saying we have connected ***/
// echo 'Connected to database<br />';


/*** set the error reporting attribute ***/
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

/*** prepare the SQL statement ***/
$stmt = $dbh->prepare("INSERT INTO $dbname.`medicament` (`medicamentID`, `medicament_name`, `unit`) VALUES (NULL, :medname, :unit);");

/*** bind the paramaters ***/
$stmt->bindParam(':medname', $medname, PDO::PARAM_STR);
$stmt->bindParam(':unit', $unit, PDO::PARAM_STR);

/*** execute the prepared statement ***/
$stmt->execute();


$sql = "SELECT * FROM medicament";
$statement = $dbh->prepare($sql);
$result = $statement->execute();
while($line = $statement->fetch()){
    echo "<option value='".$line['medicamentID']."'>".$line['medicament_name']."</option>\n";
}


/*** close the database connection ***/
$dbh = null;       
}
catch(PDOException $e)
{
  echo $e->getMessage();
}
?>