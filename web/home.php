<?php

 session_start();

// Test if the user is logged in.
// If no : back to the login page!
if(!isset($_SESSION['staffID'])){
  header('location: index.php');
  exit;
 }
?>
<html >
<head>
<title>BFH e-Health Home</title>

<link 
type="image/ico"
href="http://www.ti.bfh.ch/fileadmin/templates/img/favicon.ico?xyz=123456" 
rel="icon">
<link href="css/reset.css" rel="stylesheet" type="text/css">
<link href="css/style.css" rel="stylesheet" type="text/css">
<link href="css/structure.css" rel="stylesheet" type="text/css">
<link href="css/overlaypopup.css" rel="stylesheet" type="text/css">


<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js">
</script>
<script type="text/javascript" src="js/popup.js"></script>
<script type="text/javascript" src="js/ajaxPatientInfo.js"></script>
<script type="text/javascript" src="js/inputstyle.js"></script>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script src="http://code.highcharts.com/highcharts.js" 
type="text/javascript"></script>
<script src="http://code.highcharts.com/modules/exporting.js" 
type="text/javascript"></script>

<script>
function activateGraph(patientId){
    var nodeDiv = document.getElementById('scripts');
    var oldChild = document.getElementById('sign_graph');
    if(oldChild != null) {
	    nodeDiv.removeChild(oldChild);
    }
    var newScriptNode = document.createElement("script");
    newScriptNode.id ="sign_graph";
    newScriptNode.src = 'patientdata.php?id='+patientId;
    nodeDiv.appendChild(newScriptNode);
 }
</script>

</head>
<body onload="showSigns(1);">
<div id="wrapper">
<?php

include('pdo.inc.php');

try {
    $dbh = new PDO("mysql:host=$hostname;dbname=$dbname", $username, $password);
    
   
  echo "<!-- header -->
	<div id='header'>
		<div id='logo'> 
			<img class='headerlogo' src='img/bfh.png' name='bfh-logo' height='70px' />
		</div>	
		<div id='headertitle'>
		<h1>Homepage of ".$_SESSION['first_name']." ".$_SESSION['name']."</h1>
		</div>	
	</div>";
	
	
echo "<!-- content -->
<div id='content'>";

echo "<div id='sidebar'>";
echo "<div id='patientDropDown'>";
    echo '<h3>List of patients:</h3><br>';
    $sql = "select * from patient";

    $result = $dbh->query($sql);
    echo '<form>';
    echo '<select class="selection" name="patients" id="patients" onchange="showSigns(this.value);" >';

    while($line = $result->fetch()){
      echo "<option value=".$line['patientID'].">";
      echo $line['first_name']." ".$line['name'];
      echo "</option>";
    }
    echo '</select>';
    echo '<br>';
    echo '</form>';
echo "</div>";
    
echo "<br><br><a class='sidebarButton show-patient-popup' href='#'>Add Patient</a>";
echo "<br><a class='sidebarButton show-medicament-popup' href='#'>Add Medicament</a>";
echo "<br><a class='sidebarButton show-vitalsign-popup' href='#'>Add Vital Sign</a>";
echo "<br><a class='sidebarButton show-medicine-popup' href='#'>Add Medicine</a>";
echo "<br><br><br><a href='logout.php' class='sidebarButton'>Logout</a><br>\n";

echo "</div>";    


echo "<div id='main'>";

/*** echo a message saying we have connected ***/

echo "<div id='patientData'>";

echo "<div id='dataColumn1'></div>";
echo "<div id='dataColumn2'>";
echo "<div id='container' style='width: 500px; height: 400px; margin: 0 auto' data-highcharts-chart='0'></div>";
echo "<div id='scripts'></div>";
echo "</div>";

echo "</div>";

echo "</div>";

echo "</div>";

 $dbh = null;

}
catch(PDOException $e)
{

    /*** echo the sql statement and error message ***/
    echo $e->getMessage();
}
    
    /*** DEFINE POPUP WINDOWS
     --------------------------------------------------------------------
     ***/

    /*** Vital Sign Popup ***/

    echo "<div class='vitalsign-bg'>";
    echo "<div class='vitalsign-content'>";
    echo "<h1>Add Vital Sign</h1><br><br>";
    echo "<form action='addsign.php' method='POST' name='addSignForm'>";
    echo "<br><popuplabel>PID : </popuplabel><input type='text' id='pid' name='patientID' style='float: right;' class='inputtext' value='currentID' readonly><br>";
    echo "<br><popuplabel>Patient Name : </popuplabel><input type='text' id='pname' name='patientName' style='float: right;' class='inputtext' value='document.getElementById(\"patient\").text' readonly><br>";
        echo "<br><popuplabel>Type : </popuplabel><select id='signID' class= 'selection' name='signID' style='float: right;' >";
    include('pdo.inc.php');
    $dbh = new PDO("mysql:host=$hostname;dbname=$dbname", $username, $password);
    $sql = "SELECT * FROM sign";
    $statement = $dbh->prepare($sql);
    $result = $statement->execute();
    while($line = $statement->fetch()){
        echo "<option value='".$line['signID']."'>".$line['sign_name']."</option>\n";
        
        
    }
    echo "</select><br>";
    echo "<br><popuplabel>Value : </popuplabel><input type='text' id='val' name='val' style='float: right;' class='inputtext' ><br>\n";
    echo "<br><popuplabel>Note : </popuplabel><input type='text' id='note' name='note' style='float: right;' class='inputtext'>";
    echo "<br><br><input type='submit' class='btnSign' value='Save'>";
    echo "<input type='button' class='btnClose' value='Close'></form>";
    
    $dbh = null;
    echo "</div>";
    echo "</div>";
    
    /*** Patient Popup ***/
    
    echo "<div class='patient-bg'>";
    echo "<div class='patient-content'>";
    echo "<h1>Add Patient</h1><br><br>";
    echo "<form action='addpatient.php' method='POST' name='addPatientForm'>";
     echo "<br><popuplabel>MRN : </popuplabel><input type='text' style='float: right;' name='mrn' id='mrn' class='inputtext' value='#####'><br>\n";
    echo "<br><popuplabel>First Name : </popuplabel><input type='text' style='float: right;' name='firstname' id='firstname' value='First Name' class='inputtext'><br>\n";
    echo "<br><popuplabel>Last Name : </popuplabel><input type='text' id='lastname' style='float: right;' name='lastname' class='inputtext' value='Last Name'>";
    echo "<br><br><popuplabel>Gender : </popuplabel><select class= 'selection' id='gender' name='gender' style='float: right;'>";
    echo "<option value='1'>".male."</option>\n";
    echo "<option value='2'>".female."</option>\n";
    echo "</select><br>";
    echo "<br><popuplabel>Birthdate : </popuplabel><input id='birthdate' type='text' name='birthdate' style='float: right;' class='inputtext' value='yyyy-mm-dd'>";
    echo "<br><br><input type='submit' class='btnLogin' value='Save'>";
    echo "<input type='submit' class='btnClose' value='Close'></form>";
    echo "</div>";
    echo "</div>";
    
    /*** Medicine Popup ***/

    echo "<div class='medicine-bg'>";
    echo "<div class='medicine-content'>";
    echo "<h1>Add Medicine</h1><br><br>";
    echo "<form action='addmedicine.php' method='POST' name='addMedicineForm'>"; /*** TO DO: addmedicine.php ***/
    echo "<br><popuplabel>PID : </popuplabel><input type='text' id='pid2' name='patientID' style='float: right;' class='inputtext' value='currentID' readonly><br>";
    echo "<br><popuplabel>Patient Name : </popuplabel><input type='text' id='pname2' name='patientName' style='float: right;' class='inputtext' value='document.getElementById('patients').text' readonly><br>";
    echo "<br><popuplabel>Staff : </popuplabel><select id='staff' class= 'selection' name='staff' style='float: right;'>";
    include('pdo.inc.php');
    $dbh = new PDO("mysql:host=$hostname;dbname=$dbname", $username, $password);
    $sql = "SELECT * FROM staff";
    $statement = $dbh->prepare($sql);
    $result = $statement->execute();
    while($line = $statement->fetch()){
        echo "<option value='".$line['staffID']."'>".$line['name']." ".$line['first_name']."</option>\n";
    }
    echo "</select><br>";
    echo "<br><popuplabel>Function : </popuplabel><select id='function' class= 'selection' name='function' style='float: right;'>";
    $sql = "SELECT * FROM function";
    $statement = $dbh->prepare($sql);
    $result = $statement->execute();
    while($line = $statement->fetch()){
        echo "<option value='".$line['functionID']."'>".$line['function_name']."</option>\n";
    }
    echo "</select><br>";
    echo "<br><popuplabel>Medicine : </popuplabel><select id='medicine' class= 'selection' name='medicine' style='float: right;'>";
    $sql = "SELECT * FROM medicament";
    $statement = $dbh->prepare($sql);
    $result = $statement->execute();
    while($line = $statement->fetch()){
        echo "<option value='".$line['medicamentID']."'>".$line['medicament_name']."</option>\n";
    }
    echo "</select><br>";
    echo "<br><popuplabel>Quantity : </popuplabel><input type='text' id='quantity' name='quantity' style='float: right;' class='inputtext'><br>";
    echo "<br><popuplabel>Note : </popuplabel><input type='text' id='note2' name='note' style='float: right;' class='inputtext'>";
    echo "<br><br><input type='submit' class='btnMed' value='Save'>";
    echo "<input type='submit' class='btnClose' value='Close'></form>";
    $dbh = null;
    echo "</div>";
    echo "</div>";
    
    /*** Medicament Popup ***/

    echo "<div class='medicament-bg'>";
    echo "<div class='medicament-content'>";
    echo "<h1>Add Medicament</h1><br><br>";
    echo "<form action='addmedicament.php' method='POST' name='addMedicamentForm'>"; /*** TO DO: addmedicine.php ***/
    echo "<br><popuplabel>Medicament Name : </popuplabel><input type='text' id='medname' name='medname' style='float: right;' class='inputtext'><br>";
    echo "<br><popuplabel>Unit : </popuplabel><input type='text' id='unit' name='unit' style='float: right;' class='inputtext'>";
    echo "<br><br><input type='submit' class='btnMedicament' value='Save'>";
    echo "<input type='submit' class='btnClose' value='Close'></form>";
    echo "</div>";
    echo "</div>";

    
    

?>


<!-- footer -->
<div class='clear'> </div>
<div id="footer">	
	<p>&copy 2014 Bern University of Applied Sciences, J. Gn&aumlgi and P. Hirschi</p>
	
</div>
	
</div>

</body>
</html>