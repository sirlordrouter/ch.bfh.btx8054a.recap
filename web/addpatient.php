<?php

    session_start();
    $first = $_GET['firstname'];
    $last= $_GET['lastname'];
    $gender = $_GET['gender'];
    $birthdate = $_GET['birthdate'];
    $mrn = $_GET['mrn'];
    include('pdo.inc.php');
    
try {
    $dbh = new PDO("mysql:host=$hostname;dbname=$dbname", $username, $password);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $stmt = $dbh->prepare("INSERT INTO $dbname.`patient` (`patientID`, `MRN`, `name`, `first_name`, `gender`, `birthdate`) VALUES (NULL, :mrn, :last, :first, :gender, :birthdate)");
    
        /*** bind the paramaters ***/
    $stmt->bindParam(':mrn', $mrn, PDO::PARAM_STR);
    $stmt->bindParam(':last', $last, PDO::PARAM_STR);
    $stmt->bindParam(':first', $first, PDO::PARAM_STR);
    $stmt->bindParam(':gender', $gender, PDO::PARAM_STR);
    $stmt->bindParam(':birthdate', $birthdate, PDO::PARAM_STR);
    
    $stmt->execute();
    
    
    echo '<h3>List of patients:</h3><br>';
    $sql = "select * from patient";

    $result = $dbh->query($sql);
    echo '<form>';
    echo '<select class="selection" name="patients" id="patients" onchange="showSigns(this.value);" >';

    while($line = $result->fetch()){
      echo "<option value=".$line['patientID'].">";
      echo $line['first_name']." ".$line['name'];
      echo "</option>";
    }
    
    echo '</select>';
    echo '<br>';
    echo '</form>';
    
    
    $dbh = null;
    }
catch(PDOException $e)
{
    echo $e->getMessage();
}
?>
