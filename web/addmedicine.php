<?php
  
  session_start();
  $pid = $_GET['pid'];
  $staffID= $_GET['staffID'];
  $functionID = $_GET['functionID'];
  $medicamentID = $_GET['medicamentID'];
  $quantity = $_GET['quantity'];
  $note = $_GET['note'];
  include('pdo.inc.php');
  
  try {
    $dbh = new PDO("mysql:host=$hostname;dbname=$dbname", $username, $password);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $stmt = $dbh->prepare("INSERT INTO $dbname.`medicine` (`medicineID`, `time`, `quantity`, `medicamentID`, `patientID`, `staffID_nurse`, `staffID_physician`, `note`) VALUES (NULL, NULL, :quantity, :medicamentID, :pid, :staffID, :functionID, :note)");
    
    /*** bind the paramaters ***/
    $stmt->bindParam(':pid', $pid, PDO::PARAM_INT);
    $stmt->bindParam(':staffID', $staffID, PDO::PARAM_INT);
    $stmt->bindParam(':functionID', $functionID, PDO::PARAM_INT);
    $stmt->bindParam(':medicamentID', $medicamentID, PDO::PARAM_INT);
    $stmt->bindParam(':quantity', $quantity, PDO::PARAM_INT);
    $stmt->bindParam(':note', $note, PDO::PARAM_STR);
    
    $stmt->execute();
    $dbh = null;
  }
  catch(PDOException $e)
  {
      echo $e->getMessage();
  }
?>
