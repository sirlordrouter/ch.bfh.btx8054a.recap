<?php
 
session_start();
$val = $_GET['val'];
$signID= (int)$_GET['signID'];
$patientID = (int)$_GET['patientID'];
$note = $_GET['note'];
include('pdo.inc.php');

try {
  $dbh = new PDO("mysql:host=$hostname;dbname=$dbname", $username, $password);
  /*** echo a message saying we have connected ***/
  // echo 'Connected to database<br />';


  /*** set the error reporting attribute ***/
  $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  /*** prepare the SQL statement ***/
  $stmt = $dbh->prepare("INSERT INTO $dbname.`vital_sign` (`vital_signID`, `patientID`, `signID`, `value`, `time`, `note`) VALUES (NULL, :patientID, :signID, :value, CURRENT_TIMESTAMP, :note);");

  /*** bind the paramaters ***/
  $stmt->bindParam(':patientID', $patientID, PDO::PARAM_INT);
  $stmt->bindParam(':signID', $signID, PDO::PARAM_INT);
  $stmt->bindParam(':value', $val, PDO::PARAM_STR,5);
  $stmt->bindParam(':note', $note, PDO::PARAM_STR, 5);

  /*** execute the prepared statement ***/
  $stmt->execute();


  /*** close the database connection ***/
  $dbh = null;

  }
catch(PDOException $e)
{
  echo $e->getMessage();
}
?>