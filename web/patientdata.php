<?php
include('pdo.inc.php');

try {
    $dbh = new PDO("mysql:host=$hostname;dbname=$dbname", $username, $password);

    $patientID = (int)($_GET['id']);
    if($patientID >0){
    		
    $chartSigns = array("Temperature", "Pulse");
    		 		
echo   "$(function () {
        $('#container').highcharts({
            chart: { type: 'spline' },
            exporting: { enabled: false },
            credits: { enabled: false },
            title: {
              text: 'Patient Sign Data'
            },
            subtitle: {
              text: 'Signs recorded during hospital visit'
            },
            xAxis: {
              type: 'datetime',
              dateTimeLabelFormats: {
                  month: '%e. %b',
                  year: '%b'
              }
            },
            yAxis: [
          		{
              title: {
                  text: 'Temperature'
              } }, {
              title: {
                  text: 'Pulse (bpm)'
              },
              oposite: true
            }],
            tooltip: {
              formatter: function() {
                return this.series.name + 
                Highcharts.dateFormat(' %e. %b %H:%M', this.x) +': '+ this.y;
              }
            },
            
            series: [";
            
				$sql = "SELECT name, first_name, value, time, sign_name
			  FROM patient, vital_sign, sign
			  WHERE patient.patientID = vital_sign.patientID
			  AND vital_sign.signID = sign.signID
			  AND patient.patientID = :patientID
			  AND sign_name= :signName ORDER BY Time";

				$sqlCount = "SELECT count(*)
				FROM patient, vital_sign, sign
				WHERE patient.patientID = vital_sign.patientID
				AND vital_sign.signID = sign.signID
				AND patient.patientID = :patientID
				AND sign_name= :signName";
				
				$i = 0;
                                   
    		foreach($chartSigns as $sign) {
    		
    			if($i != 0) {
    				echo ",";
    			}     			
    			echo " { yAxis: $i, name: '$sign'," ;
    			echo "data: [";
    			
					$statement = $dbh->prepare($sqlCount);
					$statement->bindParam(':patientID', $patientID, PDO::PARAM_INT);
			        $statement->bindParam(':signName', $sign, PDO::PARAM_STR);
					$result = $statement->execute(); 
					$number_of_rows = $statement->fetchColumn();
  					
		        $statement = $dbh->prepare($sql);
		        $statement->bindParam(':patientID', $patientID, PDO::PARAM_INT);
		        $statement->bindParam(':signName', $sign, PDO::PARAM_STR);
		        $result = $statement->execute();
			    
					$j=1;			
          while($line = $statement->fetch()) {
            $date_time = explode(' ', $line['time']);
			
            $dateparts = explode('-', $date_time[0]);
            $year = (int)($dateparts[0]);
            $month = (int)($dateparts[1])-1;
            $day = (int)($dateparts[2]);
			
            $dateparts = explode(':', $date_time[1]);
            $hh = (int)($dateparts[0]);
            $mm = (int)($dateparts[1]);
            $ss = (int)($dateparts[2]);
			
            echo "[Date.UTC($year, $month, $day, $hh), " . $line['value'] . "]";
            if($j < $number_of_rows){
                echo ",";
            }
            $j++;
          }
					
					echo "]}";
					$i++;
    		}  		
    		echo "]";    		
    }
    
    echo "}); });";
} 
catch(PDOException $e) { 
    echo $e->getMessage();
}
    
?>