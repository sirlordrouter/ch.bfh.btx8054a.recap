var xmlHttp
var currentID

function showSigns(pid) { 
  currentID = pid;
  var url="patient.php";
  url=url+"?id="+pid;
  url=url+"?sid="+Math.random();
  ajaxQuery(url, null, stateSignsChanged);
  doTheMagic();
}

function stateSignsChanged() {
    if (xmlHttp.readyState==4){
        document.getElementById("dataColumn1").innerHTML = xmlHttp.responseText;
    }
    doTheMagic();
    activateGraph(currentID);
}

function doTheMagic() {
    document.getElementById("pid").value=currentID;
    document.getElementById("pid2").value=document.getElementById("patients").value;
    document.getElementById("pname").value=document.getElementById("patients")
    .options[document.getElementById("patients").selectedIndex].text;
    document.getElementById("pname2").value=document.getElementById("patients")
    .options[document.getElementById("patients").selectedIndex].text;
}
function ajaxQuery(url, formName, stateChange) {

	xmlHttp=GetXmlHttpObject();
	if (xmlHttp==null) {
	  alert ("Your browser does not support AJAX!");
	  return;
	} 
	url += "&sid="+Math.random();
	xmlHttp.onreadystatechange=stateChange;
	xmlHttp.open("GET",url,true);
	xmlHttp.send(null);
	
	if(formName != null ) {     
		document.forms[formName].reset();
	}
}

function statePatsChanged() {
    if (xmlHttp.readyState==4){
        document.getElementById("patientDropDown").innerHTML = xmlHttp.responseText;
    }
}

function stateMedisChanged() {
    if (xmlHttp.readyState==4){
        document.getElementById("medicine").innerHTML = xmlHttp.responseText;
    }
}

function GetXmlHttpObject()
{
var xmlHttp=null;
try
  {
  // Firefox, Opera 8.0+, Safari
  xmlHttp=new XMLHttpRequest();
  }
catch (e)
  {
  // Internet Explorer
  try
    {
    xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
    }
  catch (e)
    {
    xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
  }
return xmlHttp;
}