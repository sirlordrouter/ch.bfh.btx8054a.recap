$(document).ready(function(){
    // show popup when you click on the link
    $('.show-vitalsign-popup').click(function(event){
        event.preventDefault(); // disable normal link function so that it doesn't refresh the page
        var docHeight = $(document).height(); //grab the height of the page
        var scrollTop = $(window).scrollTop(); //grab the px value from the top of the page to where you're scrolling
        $('.vitalsign-bg').show().css({'height' : docHeight}); //display your popup and set height to the page height
        $('.vitalsign-content').css({'top': scrollTop+20+'px'}); //set the content 20px from the window top
    });
                  
    $('.show-patient-popup').click(function(event){
        event.preventDefault(); // disable normal link function so that it doesn't refresh the page
        var docHeight = $(document).height(); //grab the height of the page
        var scrollTop = $(window).scrollTop(); //grab the px value from the top of the page to where you're scrolling
        $('.patient-bg').show().css({'height' : docHeight}); //display your popup and set height to the page height
        $('.patient-content').css({'top': scrollTop+20+'px'}); //set the content 20px from the window top
    });

    $('.show-medicine-popup').click(function(event){
        event.preventDefault(); // disable normal link function so that it doesn't refresh the page
        var docHeight = $(document).height(); //grab the height of the page
        var scrollTop = $(window).scrollTop(); //grab the px value from the top of the page to where you're scrolling
        $('.medicine-bg').show().css({'height' : docHeight}); //display your popup and set height to the page height
        $('.medicine-content').css({'top': scrollTop+20+'px'}); //set the content 20px from the window top
    });
                  
    $('.show-medicament-popup').click(function(event){
        event.preventDefault(); // disable normal link function so that it doesn't refresh the page
        var docHeight = $(document).height(); //grab the height of the page
        var scrollTop = $(window).scrollTop(); //grab the px value from the top of the page to where you're scrolling
        $('.medicament-bg').show().css({'height' : docHeight}); //display your popup and set height to the page height
        $('.medicament-content').css({'top': scrollTop+20+'px'}); //set the content 20px from the window top
    });
  
    // hides the popup if user clicks anywhere outside the container
    $('.vitalsign-bg').click(function(){
        $('.vitalsign-bg').hide();
    })
    // prevents the overlay from closing if user clicks inside the popup overlay
    $('.vitalsign-content').click(function(){
        return false;
    });
                  
    // hides the popup if user clicks anywhere outside the container
    $('.medicament-bg').click(function(){
        $('.medicament-bg').hide();
    })
    // prevents the overlay from closing if user clicks inside the popup overlay
    $('.medicament-content').click(function(){
        return false;
    });
                  
    // hides the popup if user clicks anywhere outside the container
    $('.patient-bg').click(function(){
        $('.patient-bg').hide();
    })
    // prevents the overlay from closing if user clicks inside the popup overlay
    $('.patient-content').click(function(){
        return false;
    });
                  
    // hides the popup if user clicks anywhere outside the container
    $('.medicine-bg').click(function(){
        $('.medicine-bg').hide();
                            });
    // prevents the overlay from closing if user clicks inside the popup overlay
    $('.medicine-content').click(function(){
        return false;
    });
                  
    $('.btnClose').click(function(){
        hidePopupBackground();

    });
                  
    $('.btnLogin').click(function(){
			  var url="addpatient.php";
			  url=url+"?firstname="+$("#firstname")[0].value;
			  url=url+"&lastname="+$("#lastname")[0].value;
			  url=url+"&gender="+$("#gender")[0].value;
			  url=url+"&birthdate="+$("#birthdate")[0].value;
              url=url+"&mrn="+$("#mrn")[0].value;

				ajaxQuery(url, "addPatientForm", statePatsChanged);        
        hidePopupBackground();
    });
	
	
	$('.btnMed').click(function(){
		  var url="addmedicine.php";
		  url=url+"?pid="+$("#pid2")[0].value;
		  url=url+"&staffID="+$("#staff")[0].value;
		  url=url+"&functionID="+$("#function")[0].value;
		  url=url+"&medicamentID="+$("#medicine")[0].value;
      url=url+"&quantity="+$("#quantity")[0].value;
      url=url+"&note="+$("#note2")[0].value;
  
      ajaxQuery(url, "addMedicineForm", null);
      hidePopupBackground();
      showSigns(currentID);
    });
	                  
	$('.btnMedicament').click(function(){
		  var url="addmedicament.php";
		  url=url+"?medname="+ $("#medname")[0].value;
		  url=url+"&unit="+$("#unit")[0].value;
		  
		  ajaxQuery(url, "addMedicamentForm", stateMedisChanged);           
      hidePopupBackground();
    });
	    
	$('.btnSign').click(function(){
		  var url="addsign.php";
		  url=url+"?val="+$("#val")[0].value;
		  url=url+"&signID="+$("#signID")[0].value;
		  url=url+"&patientID="+$("#pid")[0].value;
		  url=url+"&note="+$("#note")[0].value;
      
      ajaxQuery(url, "addSignForm", null);     
      hidePopupBackground();
      showSigns(currentID);
	});

});

function hidePopupBackground() {
      $('.patient-bg').hide();
      $('.vitalsign-bg').hide();
      $('.medicine-bg').hide();
      $('.medicament-bg').hide();
}

