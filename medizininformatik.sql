-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Erstellungszeit: 26. Apr 2014 um 14:15
-- Server Version: 5.6.12
-- PHP-Version: 5.5.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Tabellenstruktur für Tabelle `bloc_note`
--

CREATE TABLE IF NOT EXISTS `bloc_note` (
  `bloc_noteID` int(11) NOT NULL AUTO_INCREMENT,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `staffID` int(11) NOT NULL,
  `patientID` int(11) NOT NULL,
  `note` text NOT NULL,
  PRIMARY KEY (`bloc_noteID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `credential`
--

CREATE TABLE IF NOT EXISTS `credential` (
  `credentialID` int(11) NOT NULL AUTO_INCREMENT,
  `staffID` int(11) NOT NULL,
  `hashed_password` text NOT NULL,
  `hashed_nfctag` text NOT NULL,
  PRIMARY KEY (`credentialID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Daten für Tabelle `credential`
--

INSERT INTO `credential` (`credentialID`, `staffID`, `hashed_password`, `hashed_nfctag`) VALUES
(2, 1, '5be93480bd8b743454a93dca084849202af43af5', ''),
(3, 2, 'b2ffdbeb87e8e6331d350b482b328d309bc5a321', ''),
(8, 3, '4e7615bfb1a689f634a9e979e910c13ef55c81b4', ''),
(9, 4, 'ea394d99160a69ded83dd9ea801c756f7b8d08c9', '');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `function`
--

CREATE TABLE IF NOT EXISTS `function` (
  `functionID` int(11) NOT NULL AUTO_INCREMENT,
  `function_name` varchar(30) NOT NULL,
  PRIMARY KEY (`functionID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Daten für Tabelle `function`
--

INSERT INTO `function` (`functionID`, `function_name`) VALUES
(1, 'Nurse'),
(2, 'Physician'),
(3, 'Secretary');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `medicament`
--

CREATE TABLE IF NOT EXISTS `medicament` (
  `medicamentID` int(11) NOT NULL AUTO_INCREMENT,
  `medicament_name` varchar(50) NOT NULL,
  `unit` varchar(30) NOT NULL,
  PRIMARY KEY (`medicamentID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Daten für Tabelle `medicament`
--

INSERT INTO `medicament` (`medicamentID`, `medicament_name`, `unit`) VALUES
(1, 'Aspirin 500', '500mg'),
(2, 'Glycerine', '2g'),
(3, 'Dafalgan', 'Pill'),
(4, 'Paracetamol', 'Dragee');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `medicine`
--

CREATE TABLE IF NOT EXISTS `medicine` (
  `medicineID` int(11) NOT NULL AUTO_INCREMENT,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `quantity` double NOT NULL,
  `medicamentID` int(11) NOT NULL,
  `patientID` int(11) NOT NULL,
  `staffID_nurse` int(11) NOT NULL,
  `staffID_physician` int(11) NOT NULL,
  `note` text NOT NULL,
  PRIMARY KEY (`medicineID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Daten für Tabelle `medicine`
--

INSERT INTO `medicine` (`medicineID`, `time`, `quantity`, `medicamentID`, `patientID`, `staffID_nurse`, `staffID_physician`, `note`) VALUES
(1, '2014-04-07 19:07:43', 5, 1, 1, 1, 1, 'Hello'),
(2, '2014-04-07 19:20:58', 40, 1, 2, 1, 2, ''),
(3, '2014-04-07 19:21:59', 2, 1, 2, 1, 2, ''),
(4, '2014-04-07 19:22:47', 5, 1, 2, 1, 2, ''),
(5, '2014-04-07 20:31:39', 1, 1, 1, 1, 3, ''),
(6, '2014-04-08 20:35:34', 2, 2, 11, 1, 1, ''),
(7, '2014-04-08 20:35:55', 4, 1, 11, 1, 1, ''),
(8, '2014-04-08 20:38:14', 10, 1, 11, 1, 1, ''),
(9, '2014-04-08 20:39:46', 4, 2, 11, 1, 1, ''),
(10, '2014-04-12 10:38:00', 5, 1, 3, 1, 1, 'Check Daily'),
(11, '2014-04-12 10:51:38', 1, 4, 1, 1, 1, 'Check Daily'),
(12, '2014-04-12 10:53:00', 1, 4, 1, 1, 1, 'Hello'),
(13, '2014-04-12 10:53:53', 5, 2, 1, 1, 1, 'Check Daily');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `patient`
--

CREATE TABLE IF NOT EXISTS `patient` (
  `patientID` int(11) NOT NULL AUTO_INCREMENT,
  `MRN` varchar(30) NOT NULL DEFAULT '',
  `name` varchar(30) NOT NULL DEFAULT '',
  `first_name` varchar(30) NOT NULL DEFAULT '',
  `gender` int(11) NOT NULL DEFAULT '1' COMMENT '1= male ; 2 = female',
  `birthdate` date NOT NULL,
  PRIMARY KEY (`patientID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Daten für Tabelle `patient`
--

INSERT INTO `patient` (`patientID`, `MRN`, `name`, `first_name`, `gender`, `birthdate`) VALUES
(1, '24598', 'Laurie', 'Hugh', 1, '1950-03-23'),
(2, '24610', 'Spencer', 'Jesse', 1, '1974-03-19'),
(3, '25009', 'Jacobson', 'Peter', 1, '1940-06-18'),
(4, '25566', 'Edelstein', 'Lisa', 2, '1981-05-10'),
(5, '35000', 'Person', 'Test', 1, '2014-08-09'),
(6, '11111', 'Gerster', 'Trudi', 2, '1900-03-12'),
(7, '11111222222222', 'KÃ¶nig', 'Frosch', 1, '0000-00-00'),
(8, '112234', 'Hello', 'Gugus', 1, '2021-12-02'),
(9, '44444', 'Feuer', 'Molch', 2, '1922-05-06'),
(10, '12345678', 'Graf', 'Frieda', 2, '1933-09-09'),
(11, '54321', 'Wolfskin', 'Jack', 1, '1900-03-12'),
(12, '4455', 'Hinterseer', 'Hausi', 1, '1945-12-12');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `sign`
--

CREATE TABLE IF NOT EXISTS `sign` (
  `signID` int(11) NOT NULL AUTO_INCREMENT,
  `sign_name` varchar(30) NOT NULL,
  PRIMARY KEY (`signID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Daten für Tabelle `sign`
--

INSERT INTO `sign` (`signID`, `sign_name`) VALUES
(1, 'Temperature'),
(2, 'Pulse'),
(3, 'Activity'),
(4, 'Blood pressure');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `staff`
--

CREATE TABLE IF NOT EXISTS `staff` (
  `staffID` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `name` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `fonctionID` int(11) NOT NULL,
  PRIMARY KEY (`staffID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Daten für Tabelle `staff`
--

INSERT INTO `staff` (`staffID`, `username`, `name`, `first_name`, `fonctionID`) VALUES
(1, 'house', 'House', 'Gregory', 2),
(2, 'wilson', 'Wilson', 'James', 2),
(3, 'taub', 'Taub', 'Chris', 1),
(4, 'cuddy', 'Cuddy', 'Lisa', 3);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `vital_sign`
--

CREATE TABLE IF NOT EXISTS `vital_sign` (
  `vital_signID` int(11) NOT NULL AUTO_INCREMENT,
  `patientID` int(11) NOT NULL,
  `signID` int(11) NOT NULL,
  `value` varchar(20) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `note` text NOT NULL,
  PRIMARY KEY (`vital_signID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Daten für Tabelle `vital_sign`
--

INSERT INTO `vital_sign` (`vital_signID`, `patientID`, `signID`, `value`, `time`, `note`) VALUES
(1, 1, 1, '37', '2014-03-01 07:20:21', 'Everything is in order'),
(2, 1, 1, '37.5', '2014-03-01 14:20:45', 'The patient is not so well, but the temperature is still OK'),
(3, 1, 1, '38.2', '2014-03-01 21:04:51', 'The patient is hot'),
(4, 1, 1, '39', '2014-03-02 07:22:27', ''),
(5, 1, 1, '38.2', '2014-03-02 11:32:47', ''),
(6, 1, 2, '75', '2014-03-01 07:23:47', ''),
(7, 1, 2, '85', '2014-03-01 14:24:03', ''),
(8, 1, 2, '110', '2014-03-01 21:24:25', ''),
(9, 1, 2, '90', '2014-03-02 11:24:43', ''),
(26, 1, 1, '36', '2014-04-11 07:24:45', 'Hello'),
(11, 1, 2, '80', '2014-04-05 13:18:34', 'Everything ok'),
(12, 1, 3, '15', '2014-04-07 19:16:47', 'Checked That'),
(13, 2, 1, '37.0', '2014-04-07 19:17:43', ''),
(14, 2, 1, '38', '2014-04-07 19:20:34', ''),
(15, 1, 4, '120', '2014-04-07 20:31:02', 'Sys'),
(16, 11, 2, '90', '2014-04-08 20:35:22', ''),
(17, 11, 1, '39', '2014-04-08 20:37:57', ''),
(18, 11, 1, '37', '2014-04-08 20:39:08', ''),
(19, 6, 1, '37.0', '2014-04-08 20:41:58', 'Hello'),
(20, 6, 1, '40.4', '2014-04-08 20:42:11', ''),
(21, 6, 1, '36.5', '2014-04-08 20:42:21', ''),
(22, 3, 1, '41.0', '2014-04-08 20:54:46', ''),
(23, 3, 1, '36', '2014-04-08 20:54:55', ''),
(24, 1, 1, '38', '2014-04-08 20:58:45', ''),
(25, 11, 1, '45', '2014-04-09 08:55:15', ''),
(27, 1, 2, '120', '2014-04-12 10:16:44', ''),
(28, 1, 1, '40.4', '2014-04-12 10:18:18', ''),
(29, 12, 2, '88', '2014-04-12 10:30:24', ''),
(30, 3, 2, '76', '2014-04-12 10:38:18', ''),
(31, 2, 2, '80', '2014-04-24 12:46:57', 'no'),
(32, 2, 1, '80', '2014-04-24 12:47:21', ''),
(33, 3, 2, '120', '2014-04-24 12:47:39', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
