
\select@language {ngerman}
\save@tocdepth \setcounter {tocdepth}{1}
\contentsline {chapter}{\numberline {1}Methoden und Vorgehensweisen}{2}{chapter.1}
\contentsline {section}{\numberline {1.1}Entwicklungsumgebung}{2}{section.1.1}
\contentsline {section}{\numberline {1.2}Login}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}Hauptseite}{2}{section.1.3}
\contentsline {section}{\numberline {1.4}Popups}{2}{section.1.4}
\contentsline {section}{\numberline {1.5}Anzeigen der Inhalte}{3}{section.1.5}
\contentsline {chapter}{\numberline {2}Funktionalit\"at}{4}{chapter.2}
\contentsline {section}{\numberline {2.1}Grundstruktur}{4}{section.2.1}
\contentsline {section}{\numberline {2.2}Auslesen der Popups}{6}{section.2.2}
\contentsline {section}{\numberline {2.3}Erzeugen der Grafik}{7}{section.2.3}
\contentsline {chapter}{\numberline {3}Ausblick \& Lessons learned}{9}{chapter.3}
\contentsline {chapter}{\numberline {A}Sourcecode}{10}{appendix.A}
\contentsline {section}{\numberline {A.1}PHP}{10}{section.A.1}
\contentsline {subsection}{\numberline {A.1.1}home.php}{10}{subsection.A.1.1}
\contentsline {subsection}{\numberline {A.1.2}patient.php}{15}{subsection.A.1.2}
\contentsline {subsection}{\numberline {A.1.3}patientdata.php}{18}{subsection.A.1.3}
\contentsline {subsection}{\numberline {A.1.4}addpatient.php}{20}{subsection.A.1.4}
\contentsline {subsection}{\numberline {A.1.5}addsign.php}{21}{subsection.A.1.5}
\contentsline {subsection}{\numberline {A.1.6}addmedicament.php}{22}{subsection.A.1.6}
\contentsline {subsection}{\numberline {A.1.7}addmedicine.php}{23}{subsection.A.1.7}
\contentsline {section}{\numberline {A.2}Javascript}{24}{section.A.2}
\contentsline {subsection}{\numberline {A.2.1}popup.js}{24}{subsection.A.2.1}
\contentsline {subsection}{\numberline {A.2.2}inputstyle.js}{27}{subsection.A.2.2}
\contentsline {subsection}{\numberline {A.2.3}ajaxPatientInfo.js}{28}{subsection.A.2.3}
\contentsline {section}{\numberline {A.3}CSS}{30}{section.A.3}
\contentsline {subsection}{\numberline {A.3.1}reset.css}{30}{subsection.A.3.1}
\contentsline {subsection}{\numberline {A.3.2}style.css}{32}{subsection.A.3.2}
\contentsline {subsection}{\numberline {A.3.3}structure.css}{36}{subsection.A.3.3}
\contentsline {subsection}{\numberline {A.3.4}overlaypopup.css}{42}{subsection.A.3.4}
\save@tocdepth \setcounter {tocdepth}{1}
